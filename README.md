A clunky command-line timer for contractions, to use late in pregnancy.

Mostly useful for those last few hours when yes, you really do want to time it,
and no you probably won't mind sitting there in front of a computer or phone to
hit the button.

Don't go to the hospital or birthing center too early. Don't go too late.

Note the "411" and "511" rules implemented in the app are simply rules of thumb
for when it's time to go give birth. They work for many people, but medical
professionals will always say "talk to your doctor" and tell you to follow
their specific advice.

## Alternatives
There are many existing iOS and Android apps for this purpose. 

You might use `contractIO` for the same reasons I created it, which are:

- freedom to edit this code: report and analyze exactly what you want
- some apps require too many permissions
- other apps will track you without consent
- Google/Apple don't need to know you've downloaded a pregnancy app
- ability to use on a computer rather than a smart phone
- the fun of tinkering

## Usage

It's best to record a contiguous session for at least an hour. To begin

```
cio record
```

The first time you run it it will ask to create a file in your `$HOME` directory in which to store the data. 

Tap any key to signal the start of a contraction and again to signal its end. 

#### Knowing when it's time to go
After each recorded event, the application will tell you how many of the transactions recorded in the previous hour and 15 minutes meets the 511 criterion.

#### Tip
When asked to continue, it's probably best to wait to select `y` (`n` is default) until the next contraction begins. It's easier to restart the application after an accidental `n` than it is to edit out a false record from pressing `y` accidentally. At present editing contraction data must be done manually.

To view a log of raw contraction data for a specified time period, 

```bash
cio view
```

which will prompt you to specify the time window.

## Installation
### Linux/Mac
You must have python3 with `setuptools` and `wheel` packages installed.

```bash
git clone git@gitlab.com:brendanrbrown/contractio.git
cd contractio
python -m build
```

This should give a message like

```bash
Successfully built contractIO-0.1.0.tar.gz and contractIO-0.1.0-py3-none-any.whl
```

Then install with `pip` from the `.whl` file.

```bash
pip install dist/contractIO-0.1.0-py3-none-any.whl
```

### Android
To use on Android, you'll need an app like [termux](https://termux.com/) and to install python and the dependencies for this package. Then follow the instructions for Linux/Mac. 

### iOS
In principle you should be able to follow the same instructions using the [iSH](https://apps.apple.com/us/app/ish-shell/id1436902243) app. However, I have not successfully tested this.

## Sources of information
TODO links
### When is it go-time?
TODO links
### What is a contraction?
TODO links
