"""
TODO
"""

import datetime as dt
import numpy as np


def calc_411(data, since_latest_secs = 1.25 * 60**2):
    """
    Within since_latest_secs of the last record in data, calculate the number
    of records for which start-to-end was at least 1 minute and the difference
    between that contraction and the previous was <= 4 minutes (<= 5 minutes).

    The standard recommendation for the 411 (511) rule is that contractions
    lasting at least 1 minute, with start-to-start differences of <= 4 minutes
    (5 minutes), lasts for at least 1 hour.

    Note this function is unsafe, in that it assumes the data will be sorted by
    start time, among other things. These should be ensured in the caller.
    """

    # UNSAFE: requires data to be sorted, and for data to be non-empty.
    # These ensured by caller. Alternatively, you could take the max start time
    # to guarantee it here.
    last = data[-1]

    def select_rec(r, ref, window):
        # UNSAFE: assumes ref["start"] > r["start"]
        time_diff = (ref["start"] - r["start"]).total_seconds()

        return time_diff <= window

    # filter data to start times within since_latest_secs from last
    data = [r for r in data if select_rec(r, last, since_latest_secs)]

    # output init
    out = dict(n_window = 0, n_411 = 0, n_511 = 0,
               window_start = None, window_secs = 0)

    # return default of 0 for all if len(data) < 2
    if len(data) < 2:
        return out

    # discard first obs to match diffs
    # row 0: start_to_start, 1: start_to_end
    diff_data = event_diffs(data)

    def n_condition(time_thresh):
        # indices meeting conditions
        ge_one_min = diff_data[1, :] >= 60

        # count number of cols for which *both* first and second conditions hold
        return np.array([diff_data[0, :] <= time_thresh, ge_one_min]).all(0).sum()

    # results
    out["n_window"] = diff_data.shape[1]
    out["n_411"] = n_condition(60 * 4)
    out["n_511"] =  n_condition(60 * 5)
    out["window_start"] = data[0]["start"]
    out["window_secs"] = (last["start"] - data[0]["start"]).total_seconds()

    return out

def alert_411(d):
    """Produce summary message for data in calc_411, checking for 511 condition."""

    # if n_window is the default 0, say nothing
    if d['n_window'] == 0:
        return "", "green"

    # n_511 >= n_411 by definition
    prop = d["n_511"] / d["n_window"]

    msg = f"""
    Since {d['window_start'].strftime('%I:%M:%S %p')}
    {d['n_411']} of {d['n_window']} contractions meet 411 rule
    {d['n_511']} of {d['n_window']} meet 511 rule\n
    """

    msg_color = "green"

    # 411/511 alert only if the window is at least 1h long
    if d["window_secs"] >= 60 ** 2:
        if .5 <= prop < .75:
            msg_color = "yellow"
        elif .75 <= prop < 1:
            msg_color = "red"
        elif prop >= 1:
            msg_color = "bright_red"
            msg = f"{msg}ALERT!\nConsider calling the doc, going to the birth site\n"
            # TODO write out alert state jsonl with date of alert
        else:
            pass

    return msg, msg_color


def summarize(d):
    # from event_diffs 0: start_to_start, 1: start_to_end

    if len(d) == 0:
        return "No data to summarize in chosen period."

    data = np.vstack(([r["start"] for r in d],
                    [r["end"] for r in d],
                    event_diffs(d, pad = True)
                  )).transpose()

    msg = format_table_row({}, header = True) + "".join([format_table_row(r) for r in data])

    return msg

# UTILITIES
to_seconds = np.vectorize(lambda r: r.total_seconds())
dur_seconds = np.vectorize(lambda r: (r["end"] - r["start"]).total_seconds())

def event_diffs(d, pad = False):
    # start-to-start diffs
    start_data = np.array([r["start"] for r in d])
    start_to_start = np.diff(start_data)
    start_to_start = to_seconds(start_to_start)

    # discard first obs to match diffs
    start_to_end = dur_seconds(d)

    # end_data
    if pad:
        start_to_start = np.insert(start_to_start, 0, np.nan)
    else:
        start_to_end = start_to_end[1:]

    return np.array([start_to_start, start_to_end])

def format_table_row(arr, header = False):
    """Format an array with structure start, end, start_to_start, start_to_end"""
    table_sep_small = "-" * 6
    table_sep_large = "-" * 12
    table_sep = "-" * 10

    if header:
        msg = f"|{'M-D':<6}|{'Start':<12}|{'End':<12}|{'Interval':<10}|{'Duration':<10}|\n"
        msg = f"{msg}|{table_sep_small:<6}|{table_sep_large:<12}|{table_sep_large:<12}|{table_sep:<10}|{table_sep:<10}|\n"
    else:
        msg = f"|{arr[0].strftime('%m-%d'):<6}|{arr[0].strftime('%I:%M:%S %p'):<12}|{arr[1].strftime('%I:%M:%S %p'):<12}|{format_difftime(arr[2]):<10}|{format_difftime(arr[3]):<10}|\n"

    return msg

def format_difftime(dur):
    if np.isnan(dur):
        return dur

    m = int(dur // 60)
    s = int(dur % 60)

    daymin = 60 * 24

    if m > daymin:
        d = int(m // daymin)
        m = m % daymin
        h = int(m // 60)

        msg = f"{d}d, {h}h"

    elif m > 60:
        h = int(m // 60)
        m = int(m % 60)

        msg = f"{h}h, {m}m"

    else:
        msg = f"{m}m, {s}s"

    return msg

def get_today(d):
    today = dt.date.today()
    return [r for r in d if r["start"].date() == today]

def get_week(d):
    today = dt.date.today()
    return [r for r in d if (today - r["start"].date()).days <= 7]

def get_hour(d):
    now = dt.datetime.now()
    return [r for r in d if (now - r["start"]).total_seconds() <= 60 ** 2]
