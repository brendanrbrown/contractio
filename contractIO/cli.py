"""
CLI functions
"""

import datetime as dt
import click
import contractIO.record as rec
from contractIO import analyze

@click.group()
def main():
    pass

@main.command()
def record():
    """
    Record start and end times of contractions, until you exit the program.
    Add results to .contractio/data.jsonl
    """

    # continue recording?
    cont = True

    # previous record, path to data.jsonl
    file, data = rec.check_datafile()

    while cont:
        # Indicate contraction start
        click.pause(info = "Start (press any key)")
        start = dt.datetime.now()

        click.secho(f"Contraction started at {start.strftime('%I:%M:%S %p')}", fg = "bright_blue")

        # Indicate contraction end
        click.pause(info = "End (press any key)")
        end = dt.datetime.now()

        click.secho(f"Contraction lasted until {end.strftime('%I:%M:%S %p')}", fg = "bright_blue")

        # Update data, analyze data and generate message, display result
        msg, data = rec.process_event(data, start, end)
        click.secho(msg, fg = "bright_blue")

        # Check the 411/511 conditions
        data_411 = analyze.calc_411(data)
        msg_411, col_411 = analyze.alert_411(data_411)
        click.secho(msg_411, fg = col_411)
        
        # Append to data.jsonl
        # NOTE: data is non-empty at this point
        rec.append_record(data[-1], file)

        # Continue?
        cont = click.confirm("Record another?")


@main.command()
@click.option("--window", type = click.Choice(["all", "day", "hour"]),
              prompt = "View data from ...", default = "all")
def view(window):
    """
    Produce a table of all records along with 411/511 condition, or of
    summary information.
    """

    window_opts = dict(all = lambda d: d, day = analyze.get_today, 
                       hour = analyze.get_hour)

    file, data = rec.check_datafile()
    
    data = window_opts[window](data)
    out = analyze.summarize(data)

    click.echo(out)

# Run
if "__name__" == "__main__":
    main()
