"""
Support module for `cio record` and `cio insert` commands.

These assume all time measurements are taken in the same timezone.
"""

import os
import sys
import json
from datetime import datetime
import click

def process_event(data, start, end):
    """TODO"""
    dur = (end - start).total_seconds()

    m = dur // 60
    s = dur % 60
    msg = f"Duration: {m} minutes, {s} seconds"

    data.append(dict(start = start, end = end, duration = dur))

    return msg, data

def show_dates(k, v):
    if k == "start" or k == "end":
        return str(v)
    else:
        return v

def append_data(data, file):
    """DEPRECATED: Append data to file, after converting datetime fields to string."""


    with open(file, "a") as f:
        for r in data:
            out = {k: show_dates(k, v) for k, v in r.items()}
            f.write(f"{json.dumps(out)}\n")

def append_record(r, file):
    """Append single record, a dict, to file"""

    with open(file, "a") as f:
        out = {k: show_dates(k, v) for k, v in r.items()}
        f.write(f"{json.dumps(out)}\n")

def check_datafile(within_secs_only = None):
    """
    Check whether $HOME/.contractio/data.jsonl exists. If so load and return as list
    of dicts, with start/end converted to datetime, along with the file name.
    Prompt to create one if not. By default returns only the last hour of data.
    """

    home = os.environ["HOME"]
    cio_dir = f"{home}/.contractio"

    check_ciodir(cio_dir)

    file = f"{cio_dir}/data.jsonl"

    try:
        with open(file) as f:
            d = f.readlines()

    except FileNotFoundError:
        cont = click.confirm(f"{file} does not exist. Create it?", default = True)

        if cont:
            with open(file, "x") as f:
                pass
            d = []
        else:
            print("contracIO app must have data.jsonl file. Exiting.")
            sys.exit(1)

    d = [process_rec(r) for r in d]

    if not (within_secs_only is None):
        d = [r for r in d if (datetime.now() - r["start"]).total_seconds() <= within_secs_only]

    return file, validate_data(d)

def check_ciodir(cio_dir):
    """Create cio_dir if it does not already exist."""

    try:
        os.mkdir(cio_dir)
    except FileExistsError:
        pass

def process_rec(record):
    """
    Utility to process lines from data.jsonl
    Raise an exception if end <= start for some record
    """
    r = json.loads(record)
    r["start"] = datetime.fromisoformat(r["start"])
    r["end"] = datetime.fromisoformat(r["end"])

    assert r["start"] < r["end"], f"Invalid record: start >= end\n{r}"

    return r

def validate_data(data):
    """Ensure data are sorted by start time. They can get out of order if the
    user manually edits the file or uses the (TBD) event-editing interface."""

    data.sort(key = lambda x: x["start"])

    return data
